# Translation of docs_krita_org_reference_manual___dockers___color_sliders.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-23 19:17+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/dockers/color_sliders.rst:1
msgid "Overview of the color sliders docker."
msgstr "Vista general de l'acoblador Controls lliscants del color."

#: ../../reference_manual/dockers/color_sliders.rst:11
#: ../../reference_manual/dockers/color_sliders.rst:16
msgid "Color Sliders"
msgstr "Controls lliscants del color"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Color"
msgstr "Color"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Color Selector"
msgstr "Selector de color"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Hue"
msgstr "To"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Saturation"
msgstr "Saturació"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Value"
msgstr "Valor"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Brightness"
msgstr "Brillantor"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Lightness"
msgstr "Claredat"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Intensity"
msgstr "Intensitat"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Luma"
msgstr "Luma"

#: ../../reference_manual/dockers/color_sliders.rst:11
msgid "Luminosity"
msgstr "Lluminositat"

#: ../../reference_manual/dockers/color_sliders.rst:20
msgid ""
"This docker has been removed in 4.1. It will return in some form in the "
"future."
msgstr ""
"Aquest acoblador ha estat eliminat. Tornarà d'alguna manera en el futur."

#: ../../reference_manual/dockers/color_sliders.rst:22
msgid "A small docker with Hue, Saturation and Lightness bars."
msgstr "Un petit acoblador amb les barres To, Saturació i Claredat."

#: ../../reference_manual/dockers/color_sliders.rst:25
msgid ".. image:: images/dockers/Color-slider-docker.png"
msgstr ".. image:: images/dockers/Color-slider-docker.png"

#: ../../reference_manual/dockers/color_sliders.rst:26
msgid ""
"You can configure this docker via :menuselection:`Settings --> Configure "
"Krita --> Color Selector Settings --> Color Sliders`."
msgstr ""
"Podeu configurar aquest acoblador mitjançant :menuselection:`Arranjament --> "
"Configura el Krita --> Ajustaments per al selector de color --> Controls "
"lliscants del color`."

#: ../../reference_manual/dockers/color_sliders.rst:28
msgid ""
"There, you can select which sliders you would like to see added, allowing "
"you to even choose multiple lightness sliders together."
msgstr ""
"Allà, podreu seleccionar quins controls lliscants voleu veure afegits, fins "
"i tot per a poder triar múltiples controls lliscants de claredat junts."
