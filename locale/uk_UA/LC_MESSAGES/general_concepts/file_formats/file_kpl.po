# Translation of docs_krita_org_general_concepts___file_formats___file_kpl.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_kpl\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 09:34+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_kpl.rst:1
msgid "The Krita Palette file format."
msgstr "Формат файлів палітри Krita."

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "*.kpl"
msgstr "*.kpl"

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "KPL"
msgstr "KPL"

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "Krita Palette"
msgstr "Палітра Krita"

#: ../../general_concepts/file_formats/file_kpl.rst:15
msgid "\\*.kpl"
msgstr "\\*.kpl"

#: ../../general_concepts/file_formats/file_kpl.rst:17
msgid ""
"Since 4.0, Krita has a new palette file-format that can handle colors that "
"are wide gamut, RGB, CMYK, XYZ, GRAY, or LAB, and can be of any of the "
"available bitdepths, as well as groups. These are Krita Palettes, or ``*."
"kpl``."
msgstr ""
"Починаючи з версії 4.0, у Krita передбачено новий формат файлів палітри, у "
"якому можна зберігати дані кольорів у широкому діапазоні, RGB, CMYK, XYZ, "
"GRAY або LAB, а самі кольори можуть мати довільну бітову глибину. Також "
"передбачено групування кольорів. Формат файлів має назву Krita Palettes "
"(палітри Krita) або ``*.kpl``."

#: ../../general_concepts/file_formats/file_kpl.rst:19
msgid ""
"``*.kpl`` files are ZIP files, with two XMLs and ICC profiles inside. The "
"colorset XML contains the swatches as ColorSetEntry and Groups as Group. The "
"profiles.XML contains a list of profiles, and the ICC profiles themselves "
"are embedded to ensure compatibility over different computers."
msgstr ""
"Файли ``*.kpl`` є архівами ZIP, у яких зберігаються два файли XML і профілі "
"ICC. XML набору кольорів містить зразки у записах ColorSetEntry і групи у "
"записах Group. Файл профілів profiles.XML містить список профілів, а самі "
"профілі ICC вбудовано до архіву для забезпечення сумісності із різними "
"типами комп'ютерів."
