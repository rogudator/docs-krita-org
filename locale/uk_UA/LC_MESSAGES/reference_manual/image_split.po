# Translation of docs_krita_org_reference_manual___image_split.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___image_split\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:18+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Autosave on split"
msgstr "Автоматично зберігати при поділі"

#: ../../reference_manual/image_split.rst:1
msgid "The Image Split functionality in Krita"
msgstr "Функціональна можливість поділу зображення у Krita"

#: ../../reference_manual/image_split.rst:10
msgid "Splitting"
msgstr "Поділ"

#: ../../reference_manual/image_split.rst:15
msgid "Image Split"
msgstr "Поділ зображень"

#: ../../reference_manual/image_split.rst:17
msgid ""
"Found under :menuselection:`Image --> Image Split`, the Image Split function "
"allows you to evenly split a document up into several sections. This is "
"useful for splitting up spritesheets for example."
msgstr ""
"Функціональна можливість поділу зображень, викликати яку можна за допомогою "
"пункту меню :menuselection:`Зображення --> Поділ зображення`, надає вам "
"змогу правильно поділити документ на декілька частин. Ця можливість, "
"зокрема, корисна для поділу набору спрайтів."

#: ../../reference_manual/image_split.rst:19
msgid "Horizontal Lines"
msgstr "Горизонтальні лінії"

#: ../../reference_manual/image_split.rst:20
msgid ""
"The amount of horizontal lines to split at. 4 lines will mean that the image "
"is split into 5 horizontal stripes."
msgstr ""
"Кількість горизонтальних ліній поділу. 4 лінії означатимуть, що зображення "
"буде поділено на 5 частин."

#: ../../reference_manual/image_split.rst:22
msgid "Vertical Lines"
msgstr "Вертикальні лінії"

#: ../../reference_manual/image_split.rst:22
msgid ""
"The amount of vertical lines to split at. 4 lines will mean that the image "
"is split into 5 vertical stripes."
msgstr ""
"Кількість вертикальних ліній поділу. 4 лінії означатимуть, що зображення "
"буде поділено на 5 частин."

#: ../../reference_manual/image_split.rst:24
msgid "Sort Direction"
msgstr "Напрямок упорядковування"

#: ../../reference_manual/image_split.rst:28
msgid "Whether to number the files using the following directions:"
msgstr "Визначає нумерацію файлів з використанням таких напрямків:"

#: ../../reference_manual/image_split.rst:30
msgid "Horizontal"
msgstr "Горизонтально"

#: ../../reference_manual/image_split.rst:31
msgid "Left to right, top to bottom."
msgstr "Зліва праворуч, згори вниз."

#: ../../reference_manual/image_split.rst:33
msgid "Vertical"
msgstr "Вертикально"

#: ../../reference_manual/image_split.rst:33
msgid "Top to bottom, left to right."
msgstr "Згори вниз, зліва праворуч."

#: ../../reference_manual/image_split.rst:35
msgid "Prefix"
msgstr "Префікс"

#: ../../reference_manual/image_split.rst:36
msgid ""
"The prefix at which the files should be saved at. By default this is the "
"current document name."
msgstr ""
"Префікс назв, з яким буде збережено файли. Типовим значенням префікса є "
"назва поточного документа."

#: ../../reference_manual/image_split.rst:37
msgid "File Type"
msgstr "Тип файлів"

#: ../../reference_manual/image_split.rst:38
msgid "Which file format to save to."
msgstr "Формат, у якому слід зберегти дані."

#: ../../reference_manual/image_split.rst:40
msgid ""
"This will result in all slices being saved automatically using the above "
"prefix. Otherwise Krita will ask the name for each slice."
msgstr ""
"Це означає, що усі частини буде автоматично збережено з використанням "
"вказаного вище префікса. Якщо пункт не буде позначено, Krita проситиме "
"вказати назву для кожної частини."
